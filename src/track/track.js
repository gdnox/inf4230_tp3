function Track(){
    this.finishLine;
    this.wallThickness;
    this.trackWidth;
    this.simpleTrack = [];
    this.racetrack = [];
    this.graphics = new Track_Graphics(this);
}

Track.prototype.show = function(){
    this.graphics.show();
}

Track.prototype.add = function(position){
    if(this.finishLine == null)
        this.simpleTrack.push(position);
}

Track.prototype.finish = function(trackWidth, wallThickness){
    this.trackWidth = trackWidth;
    this.wallThickness = wallThickness;
    
    this.finishLine = this.simpleTrack[0].copy();
    this.simpleTrack.push(this.finishLine);
    this.refine();    
    this.graphics.finish(trackWidth, wallThickness);

    this.trackWidth = dist(this.simpleTrack[0].x, this.simpleTrack[0].y,
                      this.graphics.innerWall[0].x, this.graphics.innerWall[0].y) / 2;
}

Track.prototype.refine = function(){
    for (var i = 0; i < this.simpleTrack.length - 1; i++) {
        var current = this.simpleTrack[i];
        var next = this.simpleTrack[i + 1];
        
        var vec = p5.Vector.sub(next, current);
        var distance = Math.floor(p5.Vector.dist(current, next));
        
        for(var j = 0; j < distance; j++){
            var factor = map(j, 0, distance, 0, 100)/100;
            this.racetrack.push(p5.Vector.add(current, p5.Vector.mult(vec, factor)));
        }
    }
}

Track.prototype.exportToJSON = function(){
    var data = {
        'width':            this.trackWidth,
        'thickness':        this.wallThickness,
        'simple_track':     []
    };

    for (var i = 0; i < this.simpleTrack.length; i++)
        data.simple_track.push(this.simpleTrack[i].toJSON());

    return data;
}

Track.prototype.importFromJSON = function(data){
    this.trackWidth = data.width;
    this.wallThickness = data.thickness;

    for (var i = 0; i < data.simple_track.length; i++) {
        this.add(createVector(
            data.simple_track[i].x,
            data.simple_track[i].y
        ));
    }
    
    this.finish(this.trackWidth, this.wallThickness);
}

Track.prototype.isFinished = function(){
    return this.finishLine != null;
}

p5.Vector.prototype.toJSON = function(){
    return {
        'x': this.x,
        'y': this.y
    };
}