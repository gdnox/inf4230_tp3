function Track_Graphics(track){
    this.trackWidth;
    this.wallThickness = 5;
    this.simpleTrack = track.simpleTrack;
    this.centroidPoint;

    this.innerWall  = [];
    this.outterWall = [];
    this.track = track;
}

Track_Graphics.prototype.show = function(){

    if(this.track.finishLine == null){
        this.showSimple();
    } else {
        this.showSimple();
        this.showFull();
    }
}

Track_Graphics.prototype.showSimple = function(){
    push();
    stroke(150);
    strokeWeight(this.wallThickness);
    noFill();
    beginShape();
    for(var i = 0; i < this.simpleTrack.length; i++){
        vertex(this.simpleTrack[i].x, this.simpleTrack[i].y);
    }
    endShape();
    pop();
}

Track_Graphics.prototype.showFull = function(){
    push();
    stroke(255);
    strokeWeight(this.wallThickness);
    noFill();
    beginShape();
    for(var i = 0; i < this.innerWall.length; i++){
        vertex(this.innerWall[i].x, this.innerWall[i].y);
    }
    endShape();
    stroke(255);
    beginShape();
    for(var i = 0; i < this.outterWall.length; i++){
        vertex(this.outterWall[i].x, this.outterWall[i].y);
    }
    endShape();
    pop();    
}



Track_Graphics.prototype.finish = function(trackWidth, wallThickness){
    this.trackWidth = 1 + trackWidth/100;
    this.wallThickness = wallThickness;

    this.simpleTrack = this.track.simpleTrack.copyVectors();
    this.centroidPoint = calculateCentroid(this.simpleTrack);
    this.innerWall = scaleTrack(this.simpleTrack.copyVectors(), this.trackWidth - 1);
    this.outterWall = scaleTrack(this.simpleTrack.copyVectors(), this.trackWidth);
}



function scaleTrack(track, factor){
    
    var originalPosition = calculateCentroid(track.copyVectors());
    var origin = createVector(0, 0);
    track = translateTrack(track, originalPosition, origin);
    
    for (var i = 0; i < track.length; i++)
        track[i].mult(factor);

    return translateTrack(track, origin, originalPosition);
}

function translateTrack(track, centroid, to){
    var translation = createVector(
        to.x - centroid.x,
        to.y - centroid.y
    );

    for (var i = 0; i < track.length; i++)
        track[i].add(translation);

    return track;
}

function calculateCentroid(track){
    var centroid = createVector(0, 0);
    var signedArea = 0;
    var tempA = 0;

    track.push(track[0]);

    for(var i = 0; i < track.length - 1; i++){
        var x0 = track[i].x;
        var y0 = track[i].y;
        var x1 = track[(i + 1)].x;
        var y1 = track[(i + 1)].y;
        
        tempA = (x0 * y1) - (x1 * y0);

        signedArea += tempA;
        centroid.x += (x0 + x1) * tempA;
        centroid.y += (y0 + y1) * tempA;
    }
    signedArea *= 0.5;
    centroid.x /= (6.0 * signedArea);
    centroid.y /= (6.0 * signedArea);

    return centroid;
}

Array.prototype.copyVectors = function(){
    var newArray = [];
    
    for (var i = 0; i < this.length; i++) {
        newArray.push(this[i].copy());
    }
    
    return newArray;
}