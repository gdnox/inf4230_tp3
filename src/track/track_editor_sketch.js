var canvas;
var finishButton;
var exportButton;
var trackWidthInput;

var fileId = 1;

var wallThicknessInput;

var track;

function setup(){
    createP("Track Width and Wall Thickness : ");
    trackWidthInput = createInput(50);
    wallThicknessInput = createInput(5);

    finishButton = createButton('finish');
    finishButton.position(trackWidthInput.x + trackWidthInput.width + wallThicknessInput.width, trackWidthInput.y);
    finishButton.mousePressed(finish);

    exportButton = createButton('export');
    exportButton.position(400, finishButton.y);
    exportButton.mousePressed(exportFunction);

    canvas = createCanvas(1920, 1080);
    canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
    canvas.mousePressed(doMousePressed);
    track = new Track();
}

function draw(){
    background(90);
    track.show();
}

function doMousePressed() {
    track.add(createVector(mouseX, mouseY));
}

function finish(){
    track.finish(trackWidthInput.value(), wallThicknessInput.value());
}

function exportFunction(){
    var jsonTrack = track.exportToJSON();
    var name = "track"+(fileId++)+".json";
    
    var downloadAnchor = document.createElement("a");
    var file = new Blob([JSON.stringify(jsonTrack)], {type: "text"});
    downloadAnchor.href = URL.createObjectURL(file);
    downloadAnchor.download = name;
    downloadAnchor.click();
}