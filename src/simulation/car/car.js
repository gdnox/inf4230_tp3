//###### CONFIG ##############
var maxSpeed = 13,
    minDrag = 0.03,
    maxDrag = 5,
    minTurningSpeed = 0.04,
    maxTurningSpeed = 0.1,
    power = 0.7,
    braking = 0.3,
    maxHistory = 200;
//############################

function Car(x, y, color, dna = null){
    this.position = createVector(x, y);
    this.velocity = createVector();
    this.acceleration = createVector();
    this.front = p5.Vector.fromAngle(radians(0));
    this.front.normalize();

    this.hitbox = 40;
    this.speed = 0;
    this.brakeLight = false;

    //this.history = []

    this.graphics = new Car_Graphics(this, color);

    //Evaluation
    this.evaluator = new Evaluator(this);
    this.crashed = false;
    if(dna != null)
        this.dna = dna;
}

Car.prototype.show = function(){
    this.graphics.show();
}

// Control functions ######################################
Car.prototype.gas = function(){
    var vec = this.front;
    vec.setMag(power);
    this.acceleration = this.acceleration.add(vec);
    this.speed += power;
    this.brakeLight = false;
}

Car.prototype.brake = function(){
    if(this.speed > 0) {
        this.speed -= braking
        this.brakeLight = true;
    }
}

Car.prototype.turn = function(sign){
    var angle = this.processTurningRate();
    angle *= sign;
    if(this.speed > 1) {
        var vec = createVector(
            this.front.x * cos(angle) - this.front.y * sin(angle),
            this.front.y * cos(angle) + this.front.x * sin(angle)
        );
    
        this.acceleration = p5.Vector.add(vec);
        this.front = p5.Vector.fromAngle(this.acceleration.heading());
        this.front.normalize();
    }
}


Car.prototype.turnLeft = function(){
    this.turn(-1);
}

Car.prototype.turnRight = function(){
    this.turn(1);
}
//########################################################

Car.prototype.update = function(){
    if(!this.crashed){
        this.saveState();
    
        this.velocity.add(this.acceleration);
        this.speed -= this.processDrag();
    
        this.speed = this.speed < 0        ? 0        : this.speed;
        this.speed = this.speed > maxSpeed ? maxSpeed : this.speed;
    
        this.velocity.setMag(this.speed);
        this.position.add(this.velocity);
    
        this.acceleration.mult(0);
    }
}

Car.prototype.saveState = function(){
    if(this.speed > 0){
        this.evaluator.update();

        /*this.history.push({
            tires: this.position.copy(),
            angle: this.front.heading()
        });

        if(this.history.length >= maxHistory)
            this.history.splice(0, 1);*/
    }
}

// track as in track.simpleTrack
Car.prototype.evaluate = function(track){
    return this.evaluator.evaluate(track);
}

/**
 * Returns the angle in rad formed by the front of the car and the direction 
 * in which it is going
 */
Car.prototype.getAngle = function(){
    return abs(this.getAngleNonAbs());
}

Car.prototype.getAngleNonAbs = function(){
    return this.front.heading() - this.velocity.heading();
}

// Physics functions ######################################
Car.prototype.processDrag = function(){
    var angle = this.getAngle();
    return map(angle, 0, 45, minDrag, maxDrag);
}

Car.prototype.processTurningRate = function(){
    if(this.speed > maxSpeed/2){
        return map(this.speed, maxSpeed/2, maxSpeed, maxTurningSpeed, minTurningSpeed);
    } else if (this.speed <= maxSpeed/2){
        return map(this.speed, 0, maxSpeed/2, minTurningSpeed, maxTurningSpeed);
    }
}
//########################################################