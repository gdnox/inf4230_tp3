//###### CONFIG ##############
var c_width = 85,
    c_height = 50,
    frontWOffset = c_width/2 - 10,
    rearWOffset = -c_width/2 + 10,
    windshieldOffset = 5,
    rearWindshieldOffset = -c_width/2 + 20,
    headlightsOffset = c_width/2 - 3,
    taillightsOffset = -c_width/2 + 3.
    turningAngle = 35;
//############################

function Car_Graphics(car, color){
    this.color = color;
    this.car = car;
    this.size = createVector(c_width, c_height);
}

Car_Graphics.prototype.show = function(){
    //this.showTireMarks();

    push();

    noStroke();
    translate(this.car.position.x, this.car.position.y);
    rotate(this.car.front.heading());
    rectMode(CENTER);
    this.wheels();
    fill(this.color.x, this.color.y, this.color.z);
    rect(0, 0, this.size.x, this.size.y, 5);

    this.windshield();
    this.rearWindshield();
    this.headlights();
    this.taillights();

    pop();
}

Car_Graphics.prototype.showTireMarks = function(){
    for(var i = 0; i < this.car.history.length; i++){
        push();
        noStroke();
        rectMode(CENTER);
        var pos = this.car.history[i].tires;
        var angle = this.car.history[i].angle;
        translate(pos.x, pos.y);
        rotate(angle);
        this.tireMark();
        pop();
    }
}

Car_Graphics.prototype.headlights = function(){
    fill(255);
    rect(headlightsOffset, -c_height/2+10, 2, 10, 10);
    rect(headlightsOffset, c_height/2-10, 2, 10, 10);
}

Car_Graphics.prototype.taillights = function(){
    fill(181, 32, 32);
    rect(taillightsOffset, -c_height/2+10, 2, 10, 10);
    rect(taillightsOffset, c_height/2-10, 2, 10, 10);
    if(this.car.brakeLight){
        fill('rgba(255, 0, 0, 0.25)');
        ellipse(taillightsOffset, -c_height/2+5, 30);
        ellipse(taillightsOffset, c_height/2-5, 30);
    }
}

Car_Graphics.prototype.windshield = function(){
    fill(39, 40, 40);
    rect(windshieldOffset, 0, 15, c_height-7, 3);
}

Car_Graphics.prototype.rearWindshield = function(){
    fill(39, 40, 40);
    rect(rearWindshieldOffset, 0, 10, c_height-10, 3);
}

Car_Graphics.prototype.tireMark = function(){
    fill(57);
    rect(frontWOffset, -c_height/2, 20, 10, 5);
    rect(frontWOffset, c_height/2, 20, 10, 5);
    rect(rearWOffset, -c_height/2, 20, 10, 5);
    rect(rearWOffset, c_height/2, 20, 10, 5);
}

Car_Graphics.prototype.wheels = function(){
    var angle = this.car.getAngleNonAbs();

    if(angle > 0.2)       this.frontWheels(true, true);
    else if(angle < -0.2) this.frontWheels(true, false);
    else                  this.frontWheels(false, false);

    this.rearWheels();
}

Car_Graphics.prototype.setDrawingStates = function(turning, right){
    if(turning && right)       rotate(radians( 30));
    else if(turning && !right) rotate(radians(-30));
}

Car_Graphics.prototype.frontWheels = function(turning, right){
    fill(39, 40, 40);
    push();
        this.setDrawingStates(turning, right);
        push();
            if(turning && right)       translate(-20, -10);
            else if(turning && !right) translate(5, 20);

            rect(frontWOffset, -c_height/2, 20, 10, 5);
        pop();
        push();
            if(turning && right)       translate(5, -20);
            else if(turning && !right) translate(-20, 10);
            
            rect(frontWOffset, c_height/2, 20, 10, 5);
        pop();
    pop();
}
Car_Graphics.prototype.width = function(){ 
    return this.c_width;
}
Car_Graphics.prototype.height = function(){ 
    return this.c_height;
}
Car_Graphics.prototype.rearWheels = function(turning, right){
    fill(39, 40, 40);
    rect(rearWOffset, -c_height/2, 20, 10, 5);
    rect(rearWOffset, c_height/2, 20, 10, 5);
};
