var car;
var importFile;
var importButton;
var track;
var population;
var collisionLayer;
var startSimulation

var isStart = false;

//P5 environment
var canvas;

function setup() {
    importFile = createFileInput(importFunction);

    startSimulation = createButton('start');
    startSimulation.position(importFile.x + importFile.width, importFile.y);
    startSimulation.mousePressed(function(){
        console.log(track.finishLine);
        for (var i = 0; i < population.cars.length; i++) {
            population.cars[i].position = track.finishLine.copy();
        };
        isStart = true;
    });
    
    frameRate(60);
    canvas = createCanvas(1920, 1080);
    canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
    var color = createVector(
        random(0, 255),
        random(0, 255),
        random(0, 255)
    );

    track = new Track();
    collisionLayer = new CollisionLayer(track);

    population = new Population(20, 300);
}

var draw = function() {
    background(90);
    track.show();
    //inputs();

    if(isStart && population.cars[0].dna.hasNext()) {
        for (var i = 0; i < population.cars.length; i++) {
            //console.log(i + "/" + population.cars.length);
            var car = population.cars[i];
            if(car != null){
                inputsFromDNA(car);
                car.show();
                car.update();
                collisionLayer.update(car)
            }
        };
    } else {
        population.evaluate(track.simpleTrack);
        resetCars();
    }
}

function inputs() {
    if (keyIsDown(UP_ARROW))
        car.gas();
    if (keyIsDown(DOWN_ARROW))
        car.brake();
    if (keyIsDown(LEFT_ARROW))
        car.turnLeft();
    if (keyIsDown(RIGHT_ARROW))
        car.turnRight();
}

function inputsFromDNA(car){
    var input = car.dna.next();
    console.log(car.dna.position + "/" + car.dna.inputs.length);
    if(input != null){
        readInput(car, input[0]);
        readInput(car, input[1]);
    }
}

function readInput(car, input){
    if(input == 1) car.gas();
    if(input == 2) car.brake();
    if(input == 3) car.turnRight();
    if(input == 4) car.turnLeft();
}

function importFunction(file){
    loadStrings(file.data, function(data){
        track = new Track();
        track.importFromJSON(JSON.parse(data));

        collisionLayer = new CollisionLayer(track);
    });
}

function resetCars(){
    if(track.finishLine != null){
        for (var i = 0; i < population.cars.length; i++) {
            population.cars[i].position = track.finishLine.copy();
        };
    }
}