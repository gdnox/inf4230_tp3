function CollisionLayer(track){
    this.track = track;

    this.innerWall = [];
    this.outterWall = [];
    if(this.track.isFinished())
        this.buildWalls();

}

CollisionLayer.prototype.buildWalls = function(){
    var racetrack = this.track.racetrack;
    var centroid = this.track.graphics.centroidPoint;
    var toOrigin = createVector(0 - centroid.x, 0 - centroid.y);
    var scaleFactor = this.track.graphics.trackWidth;

    for (var i = 0; i < racetrack.length; i++) {
        var checkpoint = racetrack[i].copy();
        var wall = p5.Vector.add(checkpoint, toOrigin);
        var iw = p5.Vector.mult(wall, scaleFactor - 1);
        var ow = p5.Vector.mult(wall, scaleFactor);
        iw = p5.Vector.add(iw, centroid);
        ow = p5.Vector.add(ow, centroid);

        this.innerWall.push(iw);
        this.outterWall.push(ow);
    };
}

CollisionLayer.prototype.update = function(car){
    if(this.track.isFinished()){
        var closestIW = this.findClosestWall(this.innerWall, car.position);
        var closestOW = this.findClosestWall(this.outterWall, car.position);

        if(this.checkCollision(closestIW, closestOW, car)){
            console.log("crashed");
            car.crashed = true;
        }
    }
    
}

CollisionLayer.prototype.displayClosestWalls = function(closestIW, closestOW){
    push();
    fill(255,0,0);
    noStroke();
    ellipse(this.innerWall[closestIW].x, this.innerWall[closestIW].y, 10);
    pop();
    push();
    fill(255,0,0);
    noStroke();
    ellipse(this.outterWall[closestOW].x, this.outterWall[closestOW].y, 10);
    pop();
}

CollisionLayer.prototype.checkCollision = function(closestIW, closestOW, car){
    var d1 = Math.abs(dist(this.innerWall[closestIW].x, this.innerWall[closestIW].y, car.position.x, car.position.y));
    var d2 = Math.abs(dist(this.outterWall[closestOW].x, this.outterWall[closestOW].y, car.position.x, car.position.y));
    
    if(d1 < car.hitbox || d2 < car.hitbox)
        return true;
    return false;
}

CollisionLayer.prototype.findClosestWall = function(wall, car){
    var minD = Number.MAX_SAFE_INTEGER;
    var minDIndex = -1;

    for(var i = 0; i < wall.length; i++){
        var d = Math.abs(dist(car.x, car.y, wall[i].x, wall[i].y));
        if (d <  minD){
            minD = d;
            minDIndex = i;
        }
    }
    return minDIndex
}