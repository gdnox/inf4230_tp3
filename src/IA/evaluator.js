function Evaluator(car){
    this.car = car;
    this.positions = [];
    this.fitness = 0;
};

Evaluator.prototype.update = function() {
    this.positions.push(this.car.position.copy());
};

Evaluator.prototype.evaluate = function(track) {
    var fit = 0;
    fit += this.distance();
    fit += this.progression(track);
    
    if(this.car.crashed)
        fit /= 100;

    this.fitness = fit;
};

Evaluator.prototype.distance = function() {
    var distance = 0;
    for (var i = 0; i < this.positions.length - 1; i++)
        distance += Math.abs(dist(this.positions[i].x, this.positions[i].y, this.positions[i+1].x, this.positions[i+1].y));
    return distance;
};

// track as in track.simpleTrack
Evaluator.prototype.progression = function(track) {
    var fitnessValue = 0;
    
    for (var i = 0; i < track.length; i++) {
        for (var j = 0; j < this.positions.length; j++) {
            var d = Math.abs(dist(this.positions[j].x, this.positions[j].y, track[i].x, track[i].y));
            if(d < 75)
                fitnessValue += 1000;
        };
    };

    return fitnessValue;
};