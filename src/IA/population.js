var cars;
var carsSize;
var matingpool;

function Population(nbCar, nbFrames) {
    this.track = track;
    this.collisionLayer = collisionLayer;

    this.poplationSize = nbCar;

    this.cars = [];
    this.matingpool = [];
    for (var i = 0; i < this.poplationSize; i++) {
        var color = createVector(
            random(0, 255),
            random(0, 255),
            random(0, 255)
        );

        this.cars.push(new Car(0, 0, color, new DNA(nbFrames)));
    }
}


Population.prototype.evaluate = function(simpleTrack) {
    if(simpleTrack.length != 0){
        var bestFitness = 0;
    
        for (var i = 0; i < this.cars.length; i++) {
            this.cars[i].evaluate(simpleTrack);
            if (this.cars[i].evaluator.fitness > bestFitness)
                bestFitness = this.cars[i].evaluator.fitness;
        }
    
        for (var i = 0; i < this.cars.length; i++) {
            this.cars[i].evaluator.fitness /= bestFitness;
        }
    
        this.matingpool = [];
        for (var i = 0; i < this.cars.length; i++) {
            var k = this.cars[i].evaluator.fitness * 10;
            for (var j = 0; j < k; j++) {
                this.matingpool.push(this.cars[i]);
            }
        }
        this.selection();
    }
}

Population.prototype.selection = function() {
    if(this.matingpool.length != 0){
        for (var i = 0; i < this.cars.length; i++) {
            var car1 = floor(random(0, this.matingpool.length-1));
            var car2 = floor(random(0, this.matingpool.length-1));
            
            
            var a = this.matingpool[car1].dna;
            var b = this.matingpool[car2].dna;

            var child = a.crossover(b);
            var color = createVector(
                random(0, 255),
                random(0, 255),
                random(0, 255)
            );

    
            this.cars[i] = new Car(0, 0, color, child);
            this.cars[i];
        }
    }
}
