/*
 * 1 = gas
 * 2 = brake
 * 3 = turn right
 * 4 = turn left
 * 5 = nothing
 */
var possibleInputs = [1, 2, 3, 4, 5, 5];
var position = 0;


function DNA(x, genes = null) {
    if(genes == null){
        this.inputs = [];
    
        for (var i = 0; i < x; i++)
            this.inputs.push(randomPair());
        this.position = 0;
    } else{
        this.inputs = genes;
        this.position = 0;
    }
}

function randomPair() {
    var returnRandom = [];
    var temp = possibleInputs.slice();
    var r = random(temp);
    returnRandom.push(r);
    temp.splice(r, 1);
    r = random(temp);
    returnRandom.push(r);

    return returnRandom;
}

DNA.prototype.next = function() {
    if (this.inputs.length > 0 && this.position < this.inputs.length) {
        var i = this.inputs[this.position];
        this.position++;
        return i;
    }
    return null;
};

DNA.prototype.hasNext = function(){
    return this.position < this.inputs.length;
}

DNA.prototype.crossover = function(other){
    var newInputs = [];

    for (var i = 0; i < this.inputs.length; i++) {
        if(i < this.inputs.length / 2)
            newInputs.push(this.inputs[i]);
        else
            newInputs.push(other.inputs[i]);
    };

    var newDNA = new DNA(0, newInputs);
    newDNA.mutation();
    return newDNA;
}

DNA.prototype.mutation = function(){
    for (var i = 0; i < this.inputs.length; i++) {
        if(random(1) < 0.01)
        this.inputs[i] = randomPair();
    };
}